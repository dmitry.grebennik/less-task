module.exports = function (grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    sourceMap: true,
                    sourceMapURL: "style.css.map"
                },
                files: {
                    "css/style.css": "less/app.less"
                }
            },
            production: {
                options: {
                    compress: 1, // Compress output by removing some whitespaces
                    optimization: 2 // Set the parser's optimization level
                },
                files: {
                    "css/style.css": "less/app.less"
                }
            }
        },
        watch: {
            options: {
                nospawn: true
            },
            styles: {
                files: ['images/icons/*.png', 'less/**/*.less'],
                tasks: ['sprite', 'less:development']
            }
        },
        sprite: {
            iconblack: {
                src: 'images/icons/*.png',
                dest: 'images/sprites/sprites.png',
                destCss: 'less/sprites/sprites.less',
                padding: 2
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-spritesmith');

    grunt.registerTask('default', ['sprite', 'less:development', 'watch']);
    grunt.registerTask('prod', ['sprite', 'less:production']);
};